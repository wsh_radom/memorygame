import QtQuick 2.0

Rectangle {
    property alias deskWidth: cardDesk.width
    property alias deskHeight: cardDesk.height

    property int lastChoose: -1
    property int steps: 0
    property int pairsLeft: 4
    property int stepsLeft: -1

    onPairsLeftChanged: {
        pairText.text = pairText.staticText + pairsLeft;
        if (pairsLeft == 0) {
            var c = Qt.createComponent("Startwindow.qml");
            var window = c.createObject(cardDesk);
            window.windowText = "Wygrałeś!\n Ustaw nową grę lub wyjdź.";
        }
    }

    onStepsChanged: {
        stepText.text = stepText.staticText + steps;
        if (stepsLeft != -1) {
            stepText.text += "/" + stepsLeft;
            if (stepsLeft <= steps) {
                var c = Qt.createComponent("Startwindow.qml");
                var window = c.createObject(cardDesk);
                window.windowText = "Przegrałeś!\n Ustaw nową grę lub wyjdź.";
            }

        }
    }

    id: cardDesk
    //domyslna szerokosc elementu
    width: 100
    //domysla wysokosc elementu
    height: 62

    function cleanDesk() {
        var i;
        for(i = 0; i < cardDesk.children.length; i++) {
            if (cardDesk.children[i].objectName === "playCard") {
                cardDesk.children[i].destroy();
            }
        }
    }

    function generateDesk(pairs) {
        cleanDesk();
        pairsLeft = pairs;

        var cardWidth = cardDesk.width;
        var cardHeight = cardDesk.height - 100;
        var widthRatio = pairs;
        var heightRatio = 2;
        if (pairs % 8 != 0 && pairs < 8)
            widthRatio = (pairs % 8);
        else if (pairs > 8)
            widthRatio = 8;
        cardWidth /= widthRatio;

        if (pairs > 8)
            heightRatio = 3;
        cardHeight /= heightRatio;

        var cardsPairTable = [];
        var i
        for (i = 0; i < pairs; i++)
            cardsPairTable[i] = 0;

        for (i = 0; i < heightRatio; i++) {
            for (var j = 0; j < widthRatio; j++) {
                var component = Qt.createComponent("Gamecard.qml");
                var card = component.createObject(cardDesk);
                var cardPlace = -1;
                do {
                    cardPlace = Math.floor(Math.random() * pairs);
                    if (cardsPairTable[cardPlace] < 2) {
                        cardsPairTable[cardPlace]++;
                    }
                    else
                        cardPlace = -1;

                } while(cardPlace == -1);
                card.objectName = "playCard";
                card.cardPair = cardPlace;
                card.cardWidth = cardWidth;
                card.cardHeight = cardHeight;
                card.x = 5 + (j * cardWidth);
                card.y = 5 + (i * cardHeight);
                card.cardImg = cardPlace + ".jpg";
            }
        }
    }

    function flipCards() {
        var i;
        for(i = 0; i < cardDesk.children.length; i++) {
            if (cardDesk.children[i].cardFlipped) {
                cardDesk.children[i].cardFlipped = false;
            }
        }
    }

    function dropCards() {
        var i;
        for(i = 0; i < cardDesk.children.length; i++)
            if (cardDesk.children[i].cardPair === lastChoose)
                cardDesk.children[i].destroy(); //visible = false;
    }

    function checkCardStatus(pairID) {
        if (pairID === cardDesk.lastChoose) {
            cardDesk.pairsLeft--;
            cardDesk.dropCards();
        }
        else
            cardDesk.flipCards();
        cardDesk.lastChoose = -1;
    }

    Timer {
        property int selectedPair: -1
        id: actionTime
        interval: 500
        onTriggered: checkCardStatus(selectedPair)
    }

    Startwindow {
        height: 450
        anchors.verticalCenter: parent.verticalCenter
    }

    Text {
        property string staticText: "Wykonano ruchów: "
        id: stepText
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        font.pointSize: 16
        text: staticText + steps
    }

    Text {
        property string staticText: "Pozostało par: "
        id: pairText
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        font.pointSize: 16
        text: staticText + pairsLeft
    }
}

