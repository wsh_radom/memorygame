// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
//import QtQuick 1.1
//MODIFIED IN 27,05,2013
import QtQuick 2.0
Item {
    id: button
    property bool buttonIsClicked: false

//variables describes text in button
    property alias textLabel: label.text
    property alias textColor: label.color
    property alias textFont: label.font
    property alias textFontFamily: label.font.family
    property alias textPointHeight: label.font.pointSize
    property alias textUnderline: label.font.underline
    property alias textItalic: label.font.italic
    property alias textBold: label.font.bold

//variables describes button
    property color buttonColor: "#FFFFFF"           //ustawia wszystkie kolory na taki sam odcien (przycisk jednolity)
    property color buttonBackColor: "#FFFFFF"
    property color buttonHighLitghtColor: "#FFFFFF"
    property color buttonClickedColor: "#FFFFFF"
    property color buttonPressedColor: "#FFFFFF"

    property color buttonBorderColor: "#FFFFFF"
    property color buttonBorderHighLigthColor: "#FFFFFF"
    property color buttonBorderClickedColor: "#FFFFFF"
    property color buttonBorderPressedColor: "#FFFFFF"
    property int buttonBorderWidth: 0

    property alias buttonGradient: buttonbody.gradient

    //property real buttonOpacity: 1

    property alias buttonWidth: button.width
    property alias buttonHeight: button.height
    property alias buttonRound: buttonbody.radius
    property alias buttonBodyOpacity: buttonbody.opacity

//variables describes image
    property bool iconShow: false
    property int iconSize: 30
    property int iconSizeX: 30
    property int iconSizeY: 30
    property alias iconSource: icon.source

//viariables describes buttonBackground
    //property bool buttonBackImageFill: false
    //property bool buttonBackImageOriginalSize: true
    property int buttonBackImageSizeOption: 3
    property string buttonBackImageSource
    property string buttonBackImageSourceOnHover
    property string buttonBackImageSourceOnClick
    property string buttonBackImageSourceOnPress
    property alias buttonBackImageSize: buttonBackImageSize
    property alias buttonBackImageWidth: buttonBodyBack.width
    property alias buttonBackImageHeight: buttonBodyBack.height
    property alias buttonBackImageShow: buttonBodyBack.visible
    //property alias buttonBackImageSource: buttonBodyBack.source

//set signals
    signal buttonClick()
    signal buttonEnter()
    signal buttonExit()
    signal doubleButtonClick()

    onButtonBackImageSourceChanged: buttonBackImageShow = true
    onIconSourceChanged: iconShow = true;
    onIconSizeChanged: changeImageSquareSize()
    onButtonColorChanged: setButtonColor()
    onButtonIsClickedChanged: {
        buttonbody.color = buttonClickedColor;
        if (buttonBorderWidth > 0)
            buttonbody.border.color = buttonBorderClickedColor;
        if (buttonIsClicked && buttonBackImageSourceOnClick.length > 0)
            buttonBodyBack.source = buttonBackImageSourceOnClick;
        else
            buttonBodyBack.source = buttonBackImageSource;
    }
    onButtonBodyOpacityChanged: {
        buttonbody.opacity = buttonOpacity;
        buttonBodyBack.opacity = buttonOpacity;
    }

    QtObject {
        id: buttonBackImageSize        
        property int originalSize: 0
        property int fillButton: 1
        property int customSize: 2
        property int notDefined: 3
    }

//writing all JavaScript functions

    //jezeli zostalo ustawione tlo graficzne dla przycisku to funkcja dostosowuje je do przycisku
    //originalSize okresla, ze przycisk bedzie wielkosci obrazka (przycisk zmieni swoj rozmiar na obrazka)
    //fillButton okresla, ze tlo wypelni caly przycisk (nie zmieni rozmiaru przycisku)
    //customSize okresla, ze tlo zachowa swoj rozmiar (jezeli bedzie mniejszy badz rowny rozmiar przycisku nie zmieni sie; gdyby przycisk byl mniejszy przyjmie rozmiar tla)
    function changeButtonBackImageSize() {
        if (buttonBackImageSizeOption == buttonBackImageSize.originalSize) {
            buttonWidth = buttonBodyBack.implicitWidth;
            buttonHeight = buttonBodyBack.implicitHeight;
            buttonBackImageWidth = buttonWidth;
            buttonBackImageHeight = buttonHeight;
            buttonBodyOpacity = 0;
        }
        if (buttonBackImageSizeOption == buttonBackImageSize.fillButton) {
            buttonBodyBack.anchors.fill = buttonbody;
            buttonBodyBack.fillMode = Image.Stretch;
        }
        if (buttonBackImageSizeOption == buttonBackImageSize.customSize) {
            if (buttonBackImageWidth > buttonWidth)
                buttonWidth = buttonBackImageWidth;
            if (buttonBackImageHeight > buttonHeight)
                buttonHeight = buttonBackImageHeight;
            buttonBodyBack.anchors.centerIn = buttonbody;
        }
    }

    //funkcja zmienia rozmiar ikony (obrazka) z przycisku na kwadratowy (x = y)
    function changeImageSquareSize() {
        iconSizeX = parseInt(iconSize);
        iconSizeY = parseInt(iconSize);
    }

    //funkcja sprawdza wielkosc obrazu (ikony) dodanej do przycisku
    //w zaleznosci od tego, czy miesci sie ona w przycisku czy nie nastepuje
    //ustawienie jej (icon) oraz napisu (label)
    //na koniec ustawiana jest wysokosc ikony
    function imageXPosition() {
        var ret_x = button.width;
        var element_width = label.width + icon.width;

        if (element_width < ret_x && iconShow) {
            ret_x /= 2;
            ret_x -= element_width / 2;
            icon.x = ret_x;
            label.x = icon.x + icon.width;
        }
        else {
            icon.anchors.centerIn = button;
            label.anchors.centerIn = button;
            if (iconShow) {
                icon.opacity = 0.5;
                label.font.bold = true;
            }
        }

        if (buttonbody.height < icon.height)
            icon.height = buttonbody.height;
        if (buttonbody.width < icon.width)
            icon.width = buttonbody.width;

    }

    //funkcja ustawia jednolity kolor przycisku dla jego wszyszystkich elementow
    function setButtonColor() {
        buttonBackColor = buttonColor;
        buttonHighLitghtColor = buttonColor;
        buttonClickedColor = buttonColor;
        buttonPressedColor = buttonColor;
        buttonBorderColor = buttonColor;
        buttonBorderHighLigthColor = buttonColor;
        buttonBorderClickedColor = buttonColor;
        buttonBorderPressedColor = buttonColor;        
    }

    width: 120//buttonWidth
    height: 50//buttonHeight


 //start to setting all of objects
    Rectangle {
        id: buttonbody

        width: button.width//120
        height: button.height//50
        radius: 0
        opacity: 1// buttonBodyOpacity
        border.color: buttonBorderColor
        border.width: buttonBorderWidth

        color: mouseHandler.pressed ? Qt.darker(buttonBackColor, 1.5) : buttonBackColor
    }

    Image {
        id: buttonBodyBack
        parent: buttonbody
        visible: false
        smooth: true
        fillMode: Image.PreserveAspectCrop
        clip: true
        opacity: buttonbody.opacity
        source: buttonBackImageSource
    }

    Image {
        id: icon
        anchors.verticalCenter: button.verticalCenter
        height: iconSizeX
        width: iconSizeY
        visible: iconShow
        smooth: true
        fillMode: Image.PreserveAspectCrop
        clip: true
    }

    Text {
        id: label
        anchors.verticalCenter: button.verticalCenter
        text: qsTr("label")
    }

    MouseArea {
        id: mouseHandler
        anchors.fill: buttonbody
        hoverEnabled: true
        onClicked: {
            buttonbody.color = buttonClickedColor;
            if (buttonBorderWidth > 0)
                buttonbody.border.color = buttonBorderClickedColor;
            if (buttonBackImageSourceOnClick.length > 0)
                buttonBodyBack.source = buttonBackImageSourceOnClick;
            //if (Qt.LeftButton)
            buttonClick();
        }
        onDoubleClicked: {
            buttonbody.color = buttonClickedColor;
            if (buttonBorderWidth > 0)
                buttonbody.border.color = buttonBorderClickedColor;
            if (buttonBackImageSourceOnClick.length > 0)
                buttonBodyBack.source = buttonBackImageSourceOnClick;
            //if (Qt.LeftButton)
            buttonClick();
        }
        onEntered: {
            buttonbody.color = buttonHighLitghtColor;
            if (buttonBorderWidth > 0)
                buttonbody.border.color = buttonBorderHighLigthColor;
            if (buttonBackImageSourceOnHover.length > 0)
                buttonBodyBack.source = buttonBackImageSourceOnHover;
            buttonEnter();

        }
        onExited: {
            if (!buttonIsClicked) {
                buttonbody.color = buttonBackColor;
                buttonBodyBack.source = buttonBackImageSource;
                if (buttonBorderWidth > 0)
                    buttonbody.border.color = buttonBorderColor;
            }
            else {
                buttonbody.color = buttonClickedColor;
                buttonBodyBack.source = buttonBackImageSource;
                if (buttonBorderWidth > 0)
                    buttonbody.border.color = buttonBorderClickedColor;
            }
            buttonExit();
        }
        onPressed: {
            if (buttonBorderWidth > 0)
                buttonbody.border.color = buttonBorderPressedColor;
            if (buttonBackImageSourceOnPress.length > 0)
                buttonBodyBack.source = buttonBackImageSourceOnPress;
        }
    }

    Component.onCompleted: {
        //if (icon.visible)
        imageXPosition();
        //if (buttonBackImageSizeOption != 3)
        changeButtonBackImageSize();
    }
}
