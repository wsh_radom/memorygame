// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
//MODIFIED IN 19,04,2013

Rectangle {
    id: spinbox

    property double value: 0.0
    property double maxValue: 100.0
    property double minValue: 0.0
    property double stepUp: 1.0

    property alias pressedButtonStepUpInterval: timetowait.interval

    property alias spinBoxBackColor: spinbox.color
    property alias buttonUpColor: buttonup.color
    property alias buttonUpImage: imageup.source

    property alias buttonDownColor: buttondown.color
    property alias contentColor: content.color
    property alias buttonDownImage: imagedown.source

    property alias textColor: textvalue.color
    property alias textPoinSize: textvalue.font.pointSize
    property alias textReadOnly: textvalue.readOnly

    property alias spinBoxRound: spinbox.radius
    property alias buttonUpRound: buttonup.radius
    property alias buttonDownRound: buttondown.radius
    property alias contentRound: content.radius

    width: 100
    height: 30
    color: "#000000"
    z: 0

    onEnabledChanged: {
        if (enabled)
            content.color = "#FFFFFF";
        else
            content.color = "#C1C1C1";
    }

    Timer {
        id: timetowait

        property bool __steppingUp: false

        interval: 200
        repeat: true

        onTriggered: {
            if (!__steppingUp)
            {
                if (textvalue.cursorVisible)
                    textvalue.cursorVisible = false;
                if (value > minValue)
                    value -= stepUp;
            }
            else
            {
                if (textvalue.cursorVisible)
                    textvalue.cursorVisible = false;
                if (value < maxValue)
                    value += stepUp;
            }
        }
    }

    Rectangle {
        id: buttonup
        width: spinbox.width / 5 //20
        height: spinbox.height / 2
        anchors.right: spinbox.right
        anchors.top: spinbox.top
        color: "#00FF00"
        z: 5

        Image {
            id: imageup
            anchors.fill: buttonup
            z:5
        }

        MouseArea {
            id: buttonuparea
            anchors.fill: buttonup
            onClicked: {
                if (textvalue.cursorVisible)
                    textvalue.cursorVisible = false;
                if (value < maxValue)
                    value += stepUp;
            }
            onPressed: {
                if (textvalue.cursorVisible)
                    textvalue.cursorVisible = false;
                timetowait.__steppingUp = true;
                timetowait.start();
            }
            onReleased: {
                timetowait.stop();
                timetowait.__steppingUp = false;
            }
        }
    }

    Rectangle {
        id: buttondown
        width: spinbox.width / 5 //20
        height: spinbox.height / 2
        anchors.right: spinbox.right
        anchors.bottom: spinbox.bottom
        color: "#FF0000"
        z: 5

        Image {
            id: imagedown
            anchors.fill: buttondown
            z: 5
        }

        MouseArea {
            id: buttondownarea
            anchors.fill: buttondown
            onClicked: {
                if (textvalue.cursorVisible)
                    textvalue.cursorVisible = false;
                if (value > minValue)
                    value -= stepUp;
            }
            onPressed: {
                if (textvalue.cursorVisible)
                    textvalue.cursorVisible = false;
                timetowait.__steppingUp = false;
                timetowait.start();
            }
            onReleased: {
                timetowait.stop();
                timetowait.__steppingUp = false;
            }
        }
    }

    Rectangle {
        id: content
        width: spinbox.width - spinbox.width / 5
        height: spinbox.height
        anchors.left: spinbox.left
        color: "#FFFFFF"
        border.width: spinbox.border.width
        border.color: spinbox.border.color
        z: 1
    }

    onStepUpChanged: {
        if (stepUp.toString().split('.').length > 1)
            textvalue.decimalFix = stepUp.toString().split('.')[1].length;
    }

    onValueChanged: {
        if (value < minValue)
            value = minValue;
        if (value > maxValue)
            value = maxValue;
        textvalue.text = value.toFixed(textvalue.decimalFix);
    }

    TextInput {
        id: textvalue

        property int decimalFix: 0
        width: content.width
        anchors.margins: 5
        anchors.verticalCenter: content.verticalCenter
        anchors.horizontalCenter: content.horizontalCenter
        horizontalAlignment: TextInput.AlignHCenter
        font.pixelSize: content.height - 12
        selectByMouse: true
        mouseSelectionMode: TextInput.SelectWords
        z: 2
        validator: DoubleValidator{ }
        clip: true
        color: textColor

        text: value
        cursorVisible: false

        onTextChanged: {
            if (text.charAt(0) == '0' && text.charAt(1) != '.' && value != 0)
                text = text.split('0')[1];
            if (text.split('.').length > 1)
                textvalue.decimalFix = text.split('.')[1].length;
        }

        onCursorVisibleChanged: {
            if (cursorVisible && text == "0")
                text = "";
            if (!cursorVisible)
            {
                if (text == "")
                    text = "0";
                textvalue.text = textvalue.text.replace(",", ".");
                value = parseFloat(textvalue.text);

            }
        }

        Keys.onPressed: (event) => {
            if (event.key === Qt.Key_Enter || event.key === Qt.Key_Return)
            {
                focus = false;
                cursorVisible = false;
            }
            if (text.toLowerCase() == "nan")
            {
                text = "0";
            }
        }
    }
}
