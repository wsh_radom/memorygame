import QtQuick 2.0

Rectangle {
    property alias windowText: infoText.text
    width: 700
    height: 550
    anchors.centerIn: parent

    border.color: "#000000"
    border.width: 2

    Text {
        id: infoText
        font.pointSize: 24
        anchors.horizontalCenter: parent.horizontalCenter
        y: 20
        text: "Witaj w grze\nWybierz tryb gry:"
    }

    Text {
        x: 20
        anchors.top: infoText.bottom
        text: "Ilość par w grze:"
    }

    SpinBox {
        id: spinCards
        anchors.top: infoText.bottom
        x: 270
        stepUp: 2
        maxValue: 12
        minValue: cardDesk.pairsLeft
        value: cardDesk.pairsLeft
        buttonUpColor: "#FFFFFF"
        buttonDownColor: "#FFFFFF"
        buttonUpImage: "up_arrow.png"
        buttonDownImage: "down_arrow.png"
    }

    Text {
        x: 20
        anchors.top: spinCards.bottom
        text: "Ilość ruchów w grze (0 - nielimitowane):"
    }

    SpinBox {
        id: spinMoves
        anchors.top: spinCards.bottom
        x: 270
        stepUp: 1
        minValue: 0
        value: cardDesk.pairsLeft * 2
        buttonUpColor: "#FFFFFF"
        buttonDownColor: "#FFFFFF"
        buttonUpImage: "up_arrow.png"
        buttonDownImage: "down_arrow.png"
    }

    Button {
        x: 20
        y: parent.height - buttonHeight - 30
        textLabel: "Ok"
        buttonHighLitghtColor: "#001ACC"
        onButtonClick: {
            if (spinMoves.value == 0)
                cardDesk.stepsLeft = -1;
            else if (spinMoves.value < spinCards.value)
                cardDesk.stepsLeft = spinCards.value * 2;
            else
                cardDesk.stepsLeft = spinMoves.value;
            cardDesk.steps = 0;
            cardDesk.generateDesk(spinCards.value);
            parent.destroy();
        }

        onButtonEnter: {
            textColor = "#FFFFFF";

        }
        onButtonExit: {
            textColor = "#000000";
        }
    }

    Button {
        x: parent.width - buttonWidth - 20
        y: parent.height - buttonHeight - 30
        textLabel: "Wyjdź"
        buttonHighLitghtColor: "#001ACC"
        onButtonClick: Qt.quit()
        onButtonEnter: {
            textColor = "#FFFFFF";
        }
        onButtonExit: {
            textColor = "#000000";
        }
    }
}

