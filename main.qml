import QtQuick 2.4
import QtQuick.Window 2.2

Window {
    visible: true
    x: 20
    y: 20
    width: mainDesk.width
    height: mainDesk.height

    Carddesk {
        id: mainDesk
        deskHeight: 600
        deskWidth: 800

    }
}
