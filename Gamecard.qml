import QtQuick 2.0

Rectangle {
    property alias cardImg: flipFront.source
    property alias cardWidth: flipable1.width
    property alias cardHeight: flipable1.height
    property bool cardFlipped: false

    property int cardPair

    id: cardItem
    width: flipable1.width
    height: flipable1.height
    x: 0
    y: 0

    Flipable {
        id: flipable1
        width: 200
        height: 250

        front: Image {
            id: flipFront
            anchors.fill: parent
        }

        back: Image {
            anchors.fill: parent
            source: "back.jpg"
        }

        MouseArea {
            id: mouseArea1
            anchors.fill: parent

            onClicked: {
                cardDesk.steps++;
                cardItem.cardFlipped = !cardItem.cardFlipped;
                if (cardDesk.lastChoose === -1)
                    cardDesk.lastChoose = cardItem.cardPair;
                else if (cardItem.cardFlipped) {
                    actionTime.selectedPair = cardItem.cardPair;
                    actionTime.start();
                }
                else
                    cardDesk.lastChoose = -1;
            }
        }

        transform: Rotation {
            id: rotation
            origin.x: flipable1.width/2
            origin.y: flipable1.height/2
            axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
            angle: 180
        }

        states: State {
               name: "flipCard"
               PropertyChanges { target: rotation; angle: 0 }
               when: cardItem.cardFlipped
        }

        transitions: Transition {
                NumberAnimation { target: rotation; property: "angle"; duration: 400 }
        }

    }
}

